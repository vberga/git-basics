# GIT SURVIVAL GUIDE

## Must know commands

### Clone Git repo
    git clone (origin)

### Show git repo status
    git status

**Use it whenever you want to see the status of the files under git repo folder**

**Git File Status**: untracked | unmodified | modified | staged

### Update local repository (Don't forget to pull before coding, merge files, push...)

    git pull (git fetch + git merge)

### Manage local branches

Never work on master(trunk), aways create a branch to do your work and merge the code to master after the feature/fix is finished and tested

List branches

    git branch (local branches)
    git branch -r (remote branches)

if your branch doesn't exists in remote, create a local branch

    git branch STORY/STARSMNT-XXXX

Want to delete your local branch?

    git branch -D STORY/STARSMNT-XXXX

if branch already exists in remote you can checkout a remote branch

    git checkout -t origin/STORY/STARSMNT-XXXX

Note: `-t` param and full branch name (remote/branch-name) are needed to checkout a remote branch just for the first time, after that you can checkout as following:

Changing between branches

    git checkout STORY/STARSMNT-XXXX

### Manage Git Index

**Staging area** is where you setup your commit. After create/delete/edit files under git repo you need to tell git which modifications are going into the commit, we do this by adding the modifications to the *staging area*

Add/Remove files to staging area

    git add fileName (add individual file)
    git add -A (add all files)
    git rm fileName (remove file from git and working copy)

Commit (After setup the staging area)

    git commit -m "STARSMNT-XXXX Added clean field functionality"

Tip: Do as many commits as you want, committing things in Git is fast and local

Forgot something?

    git commit -amend -m "STARSMNT-XXXX Added clean field functionality and form reload"

Add/Update remote branch - Git Push

    git push origin branch

### Merge branches

How do I merge my work into another branch?

Let's say we want to merge branch **A into B** (A -> B)

1. Checkout target branch(B) `git checkout B`
2. Merge branch A into B `git merge A`

### Reset
Ooooops... Something went wrong?

    git reset --hard

### Stashing
Want to checkout another branch quickly but Git is complaining about unstaged changed files?

Stash your half-done work using `git stash` and *unstash* the changes whenever you need

    git stash (stash your work)
    git stash pop (unstash your work)

### Workflow

## Common workflows when using Git
git pull before every code fix

* Fix BUG
    * Pull changes (download and merge latest code)
    * Create branch
    * Do code fix
    * Commit code fix
    * Merge latest code
    * Push changes

* Fix reopened BUG
    * Pull changes (download and merge latest code)
    * Checkout remote branch
    * Update repo
    * Commit code fix
    * Merge latest code
    * Push changes

* Postpone BUG to the next release
    * Pull changes (download and merge latest code)
    * Checkout remote branch
    * Merge RELEASE branch
    * Push changes

### Reflog

    -git reflog show --no-abbrev MY-BRANCH-NAME